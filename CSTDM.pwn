																				/*
________________________________________________________________________________
				   _____   _____   _______  _____   __  __ 
				  / ____| / ____|_|__   __||  __ \ |  \/  |
				 | |     | (___ (_)  | |   | |  | || \  / |
				 | |      \___ \     | |   | |  | || |\/| |
				 | |____  ____) |_   | |   | |__| || |  | |
				  \_____||_____/(_)  |_|   |_____/ |_|  |_|
								    ____
					  Counter Strike: Team Deathmatch
					  Creado por: DragonZafiro(GM) | MexTeam |
					  2014-2015	Todos los derechos reservados.
 ________________________________________________________________________________*/                                          

#include <a_samp>
#include <zcmd>
#include <sscanf2>
#include <dini>
#include <colores>
#pragma tabsize 0
// _____________________________________________________________________________
// - Configuracion
#define MAX_SCORE 	20 // Score Maximo Antes de Cambiar de Mapa
#define AUTO_BALANCE 1 // Auto-Equilibrar los Equipos
#define TIME_MAP 	15 // Minutos Antes de Cambiar de Mapa
#define MAX_MAPS 	5 // Mapas Totales
#define START_MATCH 20// Segundos para empezar las rondas
// - Servidor
#define NombreServidor "hostname [0.3z] Counter Strike: TDM (ESP/ENG)"
#define ModeServidor "Counter Strike TDM"
// - Colores
#define COLOR_TERRORIST  0xE52829FF
#define COLOR_CTERRORIST 0x6EA1DCFF
#define COLOR_SPEC       0xC6D0D7FF
// - Equipos
#define MAX_TEAM 3
// - Carpetas
#define USUARIOS "CS_TDM/Usuarios/%s.ini"
#define CONFIGURACION "CS_TDM/Configuracion/cfg.ini"
#define MARCADOR "CS_TDM/Marcador.ini"
// - Dialogs
#define DIALOG_ARMAS 	400
#define DIALOG_IDIOMA 	401
#define DIALOG_IDIOMA_I 402
#define DIALOG_REGISTER 403
#define DIALOG_LOGIN 	404
// - Registro - Logeo
#define stringRegisterEs ""csnow"Usuario: {16C09E}%s\n"csnow"Cuenta: "goldenrod"No Registrada\n\n"csnow"Porfavor escriba su "cverde"Contrase�a "csnow"para registrarse"
#define stringRegisterEn ""csnow"User: {16C09E}%s\n"csnow"Account: "goldenrod"Not Register\n\n"csnow"Please write a "cverde"Password "csnow"to Register"
#define stringLoginEs ""csnow"Usuario: {16C09E}%s\n"csnow"Cuenta: "goldenrod"Registrada\n\n"csnow"Porfavor escriba su "cverde"Contrase�a "csnow"para ingresar"
#define stringLoginEn ""csnow"User: {16C09E}%s\n"csnow"Account: "goldenrod"Registered\n\n"csnow"Please write your "cverde"Password "csnow"to Login In"
// - Mapas
#define dust 		1
#define iceworld 	2
#define detrain 	3
#define assault 	4
#define inferno 	5

// - TextDraw
new Text:Marcador;
new file[MAX_PLAYERS];
new TimerMatch;
// _____________________________________________________________________________
// - TEAM
new MapaActual;
enum
{
	Terrorist,
	CTerrorist,
	Spec
};
enum gInfo
{
	EnPartida
};
enum tInfo
{
	TeamName[64],
	Color,
    TeamScore
};

enum Pinfo
{
	ArmasSpawn,
	cTeam,
	Jugando,
	Idioma,
	// == Stats
	Logeado,
	Contrasena[15],
	Kills,
	Deaths,
	Rank
};
new MAXMAPS;
new PartidaInfo[gInfo];
new pInfo[MAX_PLAYERS][Pinfo];
new TeamInfo[MAX_TEAM][tInfo] =
{
	{"Terrorist", COLOR_TERRORIST, 0},
	{"Counter Terrorist", COLOR_CTERRORIST, 0},
	{"Spec", COLOR_SPEC, 0}
};
new Float:TerroristDust[5][4] = {
{7807.2314,-2570.4509,12.6566,129.6284},
{7803.5015,-2561.9634,12.6566,132.8244},
{7795.9194,-2560.3323,12.6566,196.5461},
{7810.1743,-2560.6528,14.8598,100.1971},
{7804.8647,-2575.9675,12.6566,178.4353}
};
new Float:CounterTerroristDust[6][4] = {
{7693.9077,-2642.1409,18.4266,273.0085},
{7694.6587,-2652.5757,18.3766,183.0085},
{7704.5952,-2658.3176,18.3766,273.0085},
{7692.0630,-2656.2810,18.3766,358.2360},
{7705.4858,-2637.3496,18.4266,269.8125},
{7709.2925,-2658.4287,18.3766,252.0568}
};

new DialogArmas[2000],
	LastEdit[12]="02/04/15",
	LastVersion[6]="1.0";
// _____________________________________________________________________________
main()
{
	print("============= * \t* =============");
	print("Counter Strike:TDM \tEspa�ol/English");
	print("Creado por: \t\tDragonZafiro");
	printf("Ultima edici�n: \t%s",LastEdit);
	printf("Ultima versi�n: \t%s",LastVersion);
	print("============= * \t* =============");
}
// _____________________________________________________________________________
public OnGameModeInit()
{
	AddPlayerClass(272,0.0,0.0,0.0,0.0,0,0,0,0,0,0); // Terrorist
	AddPlayerClass(272,0.0,0.0,0.0,0.0,0,0,0,0,0,0); // Terrorist
	AddPlayerClass(272,0.0,0.0,0.0,0.0,0,0,0,0,0,0); // Terrorist
	AddPlayerClass(272,0.0,0.0,0.0,91.1909,0,0,0,0,0,0); // Terrorist
	AddPlayerClass(285,0.0,0.0,0.0,91.1909,0,0,0,0,0,0); // Counter Terrorist
	AddPlayerClass(285,0.0,0.0,0.0,91.1909,0,0,0,0,0,0); // Counter Terrorist
	AddPlayerClass(285,0.0,0.0,0.0,91.1909,0,0,0,0,0,0); // Counter Terrorist
	AddPlayerClass(285,0.0,0.0,0.0,0.0,0,0,0,0,0,0); // Counter Terrorist
	
	strcat(DialogArmas,"9mm\t\t\t{00EE00}500$\n");
	strcat(DialogArmas,"{FFFFFF}Silenced 9mm\t\t{00EE00}500$\n");
	strcat(DialogArmas,"{FFFFFF}Desert Eagle\t\t{00EE00}600$\n");
	strcat(DialogArmas,"{FFFFFF}Shotgun\t\t{00EE00}600$\n");
	strcat(DialogArmas,"{FFFFFF}Sawnoff Shotgun\t{00EE00}800$\n");
	strcat(DialogArmas,"{FFFFFF}Combat Shotgun\t{00EE00}800$\n");
	strcat(DialogArmas,"{FFFFFF}Micro SMG/Uzi\t\t{00EE00}800$\n");
	strcat(DialogArmas,"{FFFFFF}MP5\t\t\t{00EE00}800$\n");
	strcat(DialogArmas,"{FFFFFF}AK-47\t\t\t{00EE00}800$\n");
	strcat(DialogArmas,"{FFFFFF}M4\t\t\t{00EE00}800$\n");
	strcat(DialogArmas,"{FFFFFF}Tec-9\t\t\t{00EE00}800$\n");
	strcat(DialogArmas,"{FFFFFF}Country Rifle\t\t{00EE00}800$\n");
	strcat(DialogArmas,"{FFFFFF}Sniper Rifle\t\t{00EE00}900$");
	
	Marcador = TextDrawCreate(320.000000, 430.000000, "~r~Terrorist: ~w~%i ~y~- ~b~~h~Counter Terrorist: ~w~%i");
	TextDrawAlignment(Marcador, 2);
	TextDrawBackgroundColor(Marcador, 269488383);
	TextDrawFont(Marcador, 1);
	TextDrawLetterSize(Marcador, 0.500000, 1.799999);
	TextDrawColor(Marcador, -1);
	TextDrawSetOutline(Marcador, 1);
	TextDrawSetProportional(Marcador, 1);
	TextDrawUseBox(Marcador, 1);
	TextDrawBoxColor(Marcador, 255);
	TextDrawTextSize(Marcador, 0.000000, 633.000000);
	TextDrawSetSelectable(Marcador, 0);

	SendRconCommand(NombreServidor);
	SetGameModeText(ModeServidor);
	UsePlayerPedAnims();
	TeamInfo[Terrorist][TeamScore] = 0;
	TeamInfo[CTerrorist][TeamScore] = 0;
	MAXMAPS = random(MAX_MAPS);
	CambiarMapa(1);

    format(file, sizeof(file),"CS_TDM/Marcador.ini");
    if(!dini_Exists(file))
    {
        dini_Create(file);
        print("Marcador Creado");
        dini_IntSet(file,"Terrorist Score",TeamInfo[Terrorist][TeamScore]);
        dini_IntSet(file,"Counter Terrorist Score",TeamInfo[CTerrorist][TeamScore]);
	}
	else
	{
 		TeamInfo[Terrorist][TeamScore] = dini_Int(file, "Terrorist Score");
 		TeamInfo[CTerrorist][TeamScore] = dini_Int(file, "Counter Terrorist Score");
	}
	return 1;
}
public OnGameModeExit()
{

    format(file, sizeof(file),MARCADOR);
    dini_IntSet(file,"Terrorist Score",TeamInfo[Terrorist][TeamScore]);
    dini_IntSet(file,"Counter Terrorist Score",TeamInfo[CTerrorist][TeamScore]);
    return 1;
}
// _____________________________________________________________________________
public OnPlayerConnect(playerid)
{
	SetPlayerColor(playerid,COLOR_SPEC);
	SendDeathMessage(INVALID_PLAYER_ID,playerid,200);
	pInfo[playerid][ArmasSpawn] = 1;
	pInfo[playerid][Jugando] = 0;
	new Bienvenida[256];
	format(Bienvenida,sizeof(Bienvenida),"[INFO]: %s {FFFFFF}ah entrado a nuestro servidor.",PlayerName(playerid));
	SendClientMessageToAll(COLOR_INFO,Bienvenida);
	ShowPlayerDialog(playerid, DIALOG_IDIOMA_I, DIALOG_STYLE_MSGBOX,"{902020}IDIOMA - language","\n{FFFFFF}Por favor, selecciona tu idioma\nPlease, choose your language","English", "Espa�ol");
    ActualizarMarcador();
    
    	if(IsPlayerNPC(playerid))
	{
		if(!strcmp(PlayerName(playerid), "cj_pas", true))
		{
			SetPlayerSkin(playerid,271);
			SpawnPlayer(playerid);
			return 1;
		}
		return 1;
		}
	return 1;
}
// _____________________________________________________________________________
public OnPlayerDisconnect(playerid,reason)
{
	SendDeathMessage(INVALID_PLAYER_ID,playerid,201);
	pInfo[playerid][Jugando] = 0;
	
	format(file, sizeof(file),USUARIOS,PlayerName(playerid));
	if(dini_Exists(file))
	{
		dini_IntSet(file, "Asesinatos", pInfo[playerid][Kills]);
		dini_IntSet(file, "Muertes", pInfo[playerid][Deaths]);
		dini_IntSet(file, "Rango", pInfo[playerid][Rank]);
	}
	return 1;
}
// _____________________________________________________________________________
public OnPlayerRequestSpawn(playerid)
{
    new Equipo1 = JugadoresEnEquipo(Terrorist);
    new Equipo2 = JugadoresEnEquipo(CTerrorist);
    if(Equipo1 > Equipo2 && pInfo[playerid][cTeam] == Terrorist)
    {
        if(pInfo[playerid][Idioma] == 0)
        	GameTextForPlayer(playerid, "~y~Equipo lleno!~n~~w~Elige ~b~~h~Counter Terrorist!", 3000, 5);
        else
            GameTextForPlayer(playerid, "~y~Full Team!~n~~w~Choose ~b~~h~Counter Terrorist!", 3000, 5);
        return 0;
    }
    else if(Equipo2 > Equipo1 && pInfo[playerid][cTeam] == CTerrorist)
    {
        if(pInfo[playerid][Idioma] == 0)
        	GameTextForPlayer(playerid, "~y~Equipo lleno!~n~~w~Elige ~r~Terrorist!", 3000, 5);
		else
		    GameTextForPlayer(playerid, "~y~Full Team!~n~~w~Choose ~r~Terrorist!", 3000, 5);
        return 0;
    }
	if(pInfo[playerid][Logeado] == 0)
	{
		format(file, sizeof(file),USUARIOS,PlayerName(playerid));
		if(!dini_Exists(file))
		{
			new stringEs[256],stringEn[256];
			format(stringEs,sizeof(stringEs),stringRegisterEs,PlayerName(playerid));
			format(stringEn,sizeof(stringEn),stringRegisterEn,PlayerName(playerid));
			if(pInfo[playerid][Idioma] == 0)
				ShowPlayerDialog(playerid,DIALOG_REGISTER,DIALOG_STYLE_PASSWORD,"{0099FF}Registro",stringEs,"Registrarse","Cancelar");
			else
			    ShowPlayerDialog(playerid,DIALOG_REGISTER,DIALOG_STYLE_PASSWORD,"{0099FF}Register",stringEn,"Register","Cancel");
			PlayerPlaySound(playerid,1139,0.0,0.0,0.0);
			return 0;
		}
		else if(dini_Exists(file))
		{
			new stringEs[256],stringEn[256];
			format(stringEs,sizeof(stringEs),stringLoginEs,PlayerName(playerid));
			format(stringEn,sizeof(stringEn),stringLoginEn,PlayerName(playerid));
			if(pInfo[playerid][Idioma] == 0)
				ShowPlayerDialog(playerid,DIALOG_LOGIN,DIALOG_STYLE_PASSWORD,"{0099FF}Logeo",stringEs,"Ingresar","Cancelar");
			else
			    ShowPlayerDialog(playerid,DIALOG_LOGIN,DIALOG_STYLE_PASSWORD,"{0099FF}Login",stringEn,"Login In","Cancel");
			PlayerPlaySound(playerid,1139,0.0,0.0,0.0);
			return 0;
		}
		return 1;
	}
    return 1;
}
// _____________________________________________________________________________
public OnPlayerRequestClass(playerid, classid)
{
    if(PartidaInfo[EnPartida] == 1)
    {
        pInfo[playerid][cTeam] = Spec;
        if(pInfo[playerid][Idioma] == 0)
        	SendClientMessage(playerid,COLOR_INFO,"[INFO]: {FFFFFF}Partida en curso, espera a que termine...");
		else
			SendClientMessage(playerid,COLOR_INFO,"[INFO]: {FFFFFF}Round already started, wait until round ends...");
		pInfo[playerid][ArmasSpawn] = 0;
        SpawnPlayer(playerid);
        return 1;
	}
    SetPlayerPos(playerid, 212.744918, 1821.854125, 6.414062);
    SetPlayerFacingAngle(playerid, 249.396621);
    SetPlayerCameraLookAt(playerid, 212.744918, 1821.854125, 6.414062);
    SetPlayerCameraPos(playerid, 212.744918 + (5 * floatsin(-249.396621, degrees)), 1821.854125 + (5 * floatcos(-249.396621, degrees)), 6.414062);

	switch(classid)
	{
		case 0..3:
		{
        GameTextForPlayer(playerid, "~r~Terrorist", 500, 3);
        pInfo[playerid][cTeam] = Terrorist;
		}
		case 4..7:
		{
        GameTextForPlayer(playerid, "~b~~h~Counter Terrorist", 500, 3);
        pInfo[playerid][cTeam] = CTerrorist;
        }
	}
	return 1;
}
// _____________________________________________________________________________
public OnPlayerSpawn(playerid)
{
	if(pInfo[playerid][cTeam] == Spec)
	{
	    pInfo[playerid][cTeam] = Spec;
		SetPlayerColor(playerid,COLOR_SPEC);
		SetPlayerTeam(playerid,Spec);
		for(new i = 0;i< MAX_PLAYERS;i++)
		{
		    if(pInfo[i][Jugando] == 1 && IsPlayerConnected(i) && playerid != i)
		    {
				TogglePlayerSpectating(playerid,i);
				PlayerSpectatePlayer(playerid,i);
				return 0;
			}
		}
	}
	if(pInfo[playerid][cTeam] == Terrorist)
	{
	    pInfo[playerid][cTeam] = Terrorist;
		SetPlayerTeam(playerid, Terrorist);
		SetPlayerColor(playerid,COLOR_TERRORIST);
		GivePlayerMoney(playerid,10000);
		pInfo[playerid][Jugando] = 1;
		if(pInfo[playerid][ArmasSpawn] == 1)
		ShowPlayerDialog(playerid, DIALOG_ARMAS, DIALOG_STYLE_LIST,"Comprar Armas",DialogArmas,"Comprar", "Salir");
	}
	if(pInfo[playerid][cTeam] == CTerrorist)
	{
	    pInfo[playerid][cTeam] = CTerrorist;
		SetPlayerTeam(playerid, CTerrorist);
		SetPlayerColor(playerid,COLOR_CTERRORIST);
		GivePlayerMoney(playerid,10000);
		pInfo[playerid][Jugando] = 1;
		if(pInfo[playerid][ArmasSpawn] == 1)
		ShowPlayerDialog(playerid, DIALOG_ARMAS, DIALOG_STYLE_LIST,"Comprar Armas",DialogArmas,"Comprar", "Salir");
	}


 	SpawnPlayerMap(playerid);
	TogglePlayerControllable(playerid, 0);
	TextDrawShowForPlayer(playerid,Marcador);
    SetTimerEx("SpawnTime",3000,0,"dd",playerid);

	return 1;
}
public OnPlayerText(playerid,text[])
{
	new string[1024];
	if(IsPlayerConnected(playerid))
	{
		switch(pInfo[playerid][Idioma])
		{
			case 0: format(string,sizeof(string),"%s {FF0000}[ID:%d][ESP]: {FFFFFF}%s",PlayerName(playerid),playerid,text[0]);
	  		case 1: format(string,sizeof(string),"%s {FF0000}[ID:%d][ENG]: {FFFFFF}%s",PlayerName(playerid),playerid,text[0]);
		}
		SendClientMessageToAll(GetPlayerColor(playerid),string);
		return 0;
	}
	return 1;
}
// _____________________________________________________________________________
public OnPlayerDeath(playerid, killerid, reason)
{
    SendDeathMessage(killerid, playerid, reason);
    ResetPlayerMoney(playerid);
	    pInfo[playerid][Deaths]++;
	    pInfo[killerid][Kills]++;
	    pInfo[playerid][cTeam] = Spec;
     	if(TeamInfo[pInfo[killerid][cTeam]][TeamScore] == MAX_SCORE)
     	{
			MAXMAPS = random(MAX_MAPS);
			CambiarMapa(MAXMAPS);
			new string[256],string2[256];
			format(string,sizeof(string),"[INFO]: {FFFFFF}El equipo %s {FFFFFF}A ganado la partida!",idtoString(pInfo[killerid][cTeam],1));
			format(string2,sizeof(string2),"[INFO]: %s {FFFFFF} Has won the match!",idtoString(pInfo[killerid][cTeam],1));
			TeamInfo[Terrorist][TeamScore] = 0;
			TeamInfo[CTerrorist][TeamScore] = 0;
			for(new i=0;i<MAX_PLAYERS;i++)
			{
				if(pInfo[i][Idioma] == 0)
					SendClientMessage(i,COLOR_INFO,string);
				else
				    SendClientMessage(i,COLOR_INFO,string2);
			}
		}

		if(pInfo[playerid][Jugando] == 1)
		{
		    if(pInfo[playerid][Idioma] == 0)
				SendClientMessage(playerid,COLOR_INFO,"[INFO]: {FFFFFF}Has muerto, por favor espera a que la partida termine...");
			else
				SendClientMessage(playerid,COLOR_INFO,"[INFO]: {FFFFFF}You died, please wait until round ends");
    		SetPlayerColor(playerid,COLOR_SPEC);
    		pInfo[playerid][cTeam] = Spec;
    		pInfo[playerid][Jugando] = 0;
    		SetPlayerTeam(playerid,Spec);
			TogglePlayerSpectating(playerid,1);
			PlayerSpectatePlayer(playerid,killerid);
		}

		if (JugadoresEnEquipo(Terrorist) >= 1 && JugadoresEnEquipo(CTerrorist) == 0)
		{
	  		TeamInfo[Terrorist][TeamScore]++;
	  		PartidaInfo[EnPartida] = 0;
	  		ActualizarMarcador();
	  		TimerMatch = SetTimer("StartMatch",START_MATCH*1000,1);
   			for(new i = 0; i< MAX_PLAYERS; i++)
		    {
		 		pInfo[i][cTeam] = Spec;
				pInfo[i][Jugando] = 0;
				TextDrawHideForPlayer(i,Marcador);
			 	pInfo[i][ArmasSpawn] = 1;
			 	TogglePlayerSpectating(i, 0);
	  			SetPlayerColor(i,COLOR_SPEC);
	    		pInfo[i][Jugando] = 0;
			 	SetPlayerHealth(i,0.0);
			 	ForceClassSelection(i);
			 	pInfo[i][ArmasSpawn] = 1;
    			if(pInfo[i][Idioma] == 0)
					SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}El equipo {E52828}TERRORIST {FFFFFF}Ah ganado la ronda!");
				else
					SendClientMessage(i,COLOR_INFO,"[INFO]: {E52828}TERRORIST {FFFFFF}Team has won the round!");
			}
			return 0;
		}
	 	if (JugadoresEnEquipo(CTerrorist) >= 1 && JugadoresEnEquipo(Terrorist) == 0)
		{
			TeamInfo[CTerrorist][TeamScore]++;
	  		PartidaInfo[EnPartida] = 0;
	  		ActualizarMarcador();
	  		TimerMatch = SetTimer("StartMatch",START_MATCH*1000,1);

	    	for(new i = 0; i< MAX_PLAYERS; i++)
		    {
				pInfo[i][cTeam] = Spec;
				pInfo[i][Jugando] = 0;
				TextDrawHideForPlayer(i,Marcador);
			 	pInfo[i][ArmasSpawn] = 1;
			 	TogglePlayerSpectating(i, 0);
	  			SetPlayerColor(i,COLOR_SPEC);
	    		pInfo[i][Jugando] = 0;
			 	SetPlayerHealth(i,0.0);
			 	ForceClassSelection(i);
			 	pInfo[i][ArmasSpawn] = 1;
		    	if(pInfo[i][Idioma] == 0)
					SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}El equipo {6EA1DC}COUNTER TERRORIST {FFFFFF}Ah ganado la ronda!");
				else
					SendClientMessage(i,COLOR_INFO,"[INFO]: {6EA1DC}COUNTER TERRORIST {FFFFFF}Team has won the round!");
			}
			return 0;
		}
	 	if (JugadoresEnEquipo(CTerrorist) == 0 && JugadoresEnEquipo(Terrorist) == 0)
		{
	  		TeamInfo[CTerrorist][TeamScore]++;
	  		TeamInfo[Terrorist][TeamScore]++;
	  		PartidaInfo[EnPartida] = 0;
	  		ActualizarMarcador();
	  		TimerMatch = SetTimer("StartMatch",START_MATCH*1000,1);

	    	for(new i = 0; i< MAX_PLAYERS; i++)
		    {
				pInfo[i][cTeam] = Spec;
				pInfo[i][Jugando] = 0;
				TextDrawHideForPlayer(i,Marcador);
			 	pInfo[i][ArmasSpawn] = 1;
			 	TogglePlayerSpectating(i, 0);
	  			SetPlayerColor(i,COLOR_SPEC);
	    		pInfo[i][Jugando] = 0;
			 	SetPlayerHealth(i,0.0);
			 	ForceClassSelection(i);
			 	pInfo[i][ArmasSpawn] = 1;
		    	if(pInfo[i][Idioma] == 0)
					SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Ah ocurrido un empate!");
				else
					SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Draw Game!");
			}
			return 0;
		}
    return 1;
}
// _____________________________________________________________________________
CMD:comandos(playerid, params[])
{
	ConnectNPC("cj_pas","Mission_1");
	return 1;
}
CMD:idioma(playerid,params[])
{
	ShowPlayerDialog(playerid, DIALOG_IDIOMA, DIALOG_STYLE_MSGBOX,"{902020}IDIOMA - language","\n{FFFFFF}Por favor, selecciona tu idioma\nPlease, choose your language","English", "Espa�ol");
	return 1;
}
CMD:language(playerid,params[])
{
	ShowPlayerDialog(playerid, DIALOG_IDIOMA, DIALOG_STYLE_MSGBOX,"{902020}IDIOMA - language","\n{FFFFFF}Por favor, selecciona tu idioma\nPlease, choose your language","English", "Espa�ol");
	return 1;
}
// _____________________________________________________________________________
public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
    if(dialogid == DIALOG_ARMAS)
    {
        if(response) 
        {
            if(GetPlayerMoney(playerid) >= 500)
            {
	            switch(listitem)
				{
				case 0: GivePlayerWeapon(playerid, 22, 200) && GivePlayerMoney(playerid,-500);
				case 1: GivePlayerWeapon(playerid, 23, 200) && GivePlayerMoney(playerid,-500);
				case 2: GivePlayerWeapon(playerid, 24, 200) && GivePlayerMoney(playerid,-600);
				case 3: GivePlayerWeapon(playerid, 25, 200) && GivePlayerMoney(playerid,-600);
				case 4: GivePlayerWeapon(playerid, 26, 200) && GivePlayerMoney(playerid,-800);
				case 5: GivePlayerWeapon(playerid, 27, 200) && GivePlayerMoney(playerid,-800);
				case 6: GivePlayerWeapon(playerid, 28, 200) && GivePlayerMoney(playerid,-800);
				case 7: GivePlayerWeapon(playerid, 29, 200) && GivePlayerMoney(playerid,-800);
				case 8: GivePlayerWeapon(playerid, 30, 200) && GivePlayerMoney(playerid,-800);
				case 9: GivePlayerWeapon(playerid, 31, 200) && GivePlayerMoney(playerid,-800);
				case 10: GivePlayerWeapon(playerid, 32, 200) && GivePlayerMoney(playerid,-800);
				case 11: GivePlayerWeapon(playerid, 33, 200) && GivePlayerMoney(playerid,-800);
				case 12: GivePlayerWeapon(playerid, 34, 200) && GivePlayerMoney(playerid,-900);
				}
	            return ShowPlayerDialog(playerid, DIALOG_ARMAS, DIALOG_STYLE_LIST,"Comprar Armas",DialogArmas,"Comprar", "Salir");
			}
			else
			{
			    if(pInfo[playerid][Idioma] == 0)
					SendClientMessage(playerid,COLOR_INFO,"[ERROR]: {FFFFFF}Dinero insuficiente.");
				else
					SendClientMessage(playerid,COLOR_INFO,"[ERROR]: {FFFFFF}Not enough money.");
			}
		}
    }
    
    if(dialogid == DIALOG_IDIOMA_I)
    {
        if(response)
        {
            SendClientMessage(playerid,COLOR_INFO,"[INFO]: {FFFFFF}You has been choosed {00AA00}English");
            SendClientMessage(playerid,COLOR_LIGHTGREEN,"Welcome to CS:TDM, more info type: /reglas /comandos");
			pInfo[playerid][Idioma] = 1;
			ForceClassSelection(playerid);
		}
		else
		{
		    SendClientMessage(playerid,COLOR_INFO,"[INFO]: {FFFFFF}Has seleccionado {00AA00}Espa�ol");
			SendClientMessage(playerid,COLOR_LIGHTGREEN,"Bienvenido a CS:TDM, Para m�s informaci�n escribe: /reglas /comandos");
			pInfo[playerid][Idioma] = 0;
			ForceClassSelection(playerid);
		}
    }
    if(dialogid == DIALOG_IDIOMA)
    {
        if(response)
        {
            SendClientMessage(playerid,COLOR_INFO,"[INFO]: {FFFFFF}You has been choosed {00AA00}English");
			pInfo[playerid][Idioma] = 1;
		}
		else
		{
		    SendClientMessage(playerid,COLOR_INFO,"[INFO]: {FFFFFF}Has seleccionado {00AA00}Espa�ol");
			pInfo[playerid][Idioma] = 0;
		}
    }
    
	if (dialogid == DIALOG_REGISTER)
	{
	    if(response)
	    {
	        new stringEs[256],stringEn[256];
		 	if (strlen(inputtext) >= 4 && strlen(inputtext) < 14)
		 	{
			   	format(file, sizeof(file),USUARIOS,PlayerName(playerid));
		    	dini_Create(file);
				dini_Set(file, "Nombre", PlayerName(playerid) );
				dini_Set(file, "Contrase�a", inputtext);
			   	dini_IntSet(file, "Asesinatos", pInfo[playerid][Kills]);
			   	dini_IntSet(file, "Muertes", pInfo[playerid][Deaths]);
			   	dini_IntSet(file, "Rango", pInfo[playerid][Rank]);
   				pInfo[playerid][Logeado] = 1;
			   	pInfo[playerid][Rank] = 0;
			   	format(stringEs, sizeof(stringEs), "[INFO]: {FFFFFF}Tu cuenta ah sido creada satisfactoriamente!. Contrase�a: {FF0000}%s", inputtext);
                format(stringEn, sizeof(stringEn), "[INFO]: {FFFFFF}Your account has been succesful created!. Password: {FF0000}%s",inputtext);
                if(pInfo[playerid][Idioma] == 0)
					SendClientMessage(playerid,COLOR_INFO,stringEs);
				else
				    SendClientMessage(playerid,COLOR_INFO,stringEn);
		 	}
  	 		else
  	 		{
				format(stringEs,sizeof(stringEs),stringRegisterEs,PlayerName(playerid));
				format(stringEn,sizeof(stringEn),stringRegisterEn,PlayerName(playerid));
				strcat(stringEs,"\n{F97D36}[ERROR]: La contrase�a debe tener entre 4 y 13 digitos");
				strcat(stringEn,"\n{F97D36}[ERROR]: Password must be 4 - 13 numbers/letters");
				if(pInfo[playerid][Idioma] == 0)
				    ShowPlayerDialog(playerid,DIALOG_REGISTER,DIALOG_STYLE_PASSWORD,"{0099FF}Registro",stringEs,"Registrarse","Cancelar");
				else
  	 		    	ShowPlayerDialog(playerid,DIALOG_REGISTER,DIALOG_STYLE_PASSWORD,"{0099FF}Register",stringEn,"Register","Cancel");
                PlayerPlaySound(playerid,1139,0.0,0.0,0.0);
			}
		}
 	}
 	
	if (dialogid == DIALOG_LOGIN)
	{
	    if(response)
	    {
	        new stringEs[256],stringEn[256];
	        new password[255];
			format(stringEs,sizeof(stringEs),stringLoginEs,PlayerName(playerid));
			format(stringEn,sizeof(stringEn),stringLoginEn,PlayerName(playerid));
	   		format(file, sizeof(file),USUARIOS,PlayerName(playerid));
		   	password = dini_Get(file, "Contrase�a");
		   	if(!strlen(inputtext))
          	{
  				strcat(stringEs,"\n{F97D36}[ERROR]: Contrase�a incorrecta!");
				strcat(stringEn,"\n{F97D36}[ERROR]: Incorrect Password!");
				if(pInfo[playerid][Idioma] == 0)
				    ShowPlayerDialog(playerid,DIALOG_LOGIN,DIALOG_STYLE_PASSWORD,"{0099FF}Logeo",stringEs,"Ingresar","Cancelar");
				else
        			ShowPlayerDialog(playerid,DIALOG_LOGIN,DIALOG_STYLE_PASSWORD,"{0099FF}Login",stringEn,"Login In","Cancel");
                PlayerPlaySound(playerid,1139,0.0,0.0,0.0);
				return 0;
        	}
		 	if(strcmp(password, inputtext, false) == 0)
		 	{
   				SetPlayerScore(playerid,dini_Int(file,"Asesinatos"));
				pInfo[playerid][Deaths] 	= dini_Int(file,"Muertes");
				pInfo[playerid][Kills] 		= dini_Int(file,"Asesinatos");
				pInfo[playerid][Rank] 		= dini_Int(file,"Rango");
				pInfo[playerid][Logeado]	= 1;
				SpawnPlayer(playerid);
				if(pInfo[playerid][Idioma] == 0)
					SendClientMessage(playerid,COLOR_INFO,"[INFO]: {FFFFFF}Has ingresado satisfactoriamente");
				else
					SendClientMessage(playerid,COLOR_INFO,"[INFO]: {FFFFFF}You has been succesful logged in");
		 	}
  	 		else
  	 		{
				strcat(stringEs,"\n{F97D36}[ERROR]: Contrase�a incorrecta!");
				strcat(stringEn,"\n{F97D36}[ERROR]: Incorrect Password!");
				if(pInfo[playerid][Idioma] == 0)
				    ShowPlayerDialog(playerid,DIALOG_LOGIN,DIALOG_STYLE_PASSWORD,"{0099FF}Logeo",stringEs,"Ingresar","Cancelar");
				else
        			ShowPlayerDialog(playerid,DIALOG_LOGIN,DIALOG_STYLE_PASSWORD,"{0099FF}Login",stringEn,"Login In","Cancel");
                PlayerPlaySound(playerid,1139,0.0,0.0,0.0);
			}
		}
 	}
    return 1;
}
ActualizarMarcador()
{
	new string[256];
	format(string,sizeof(string), "~r~Terrorist: ~w~%d ~y~- ~b~~h~Counter Terrorist: ~w~%d",TeamInfo[Terrorist][TeamScore],TeamInfo[CTerrorist][TeamScore]);
	TextDrawSetString(Marcador,string);
}
// _____________________________________________________________________________

stock CambiarMapa(mapa)
{
	switch(mapa)
	{
		case dust:
		{
	 		MapaActual = dust;
	 		for(new i=0;i<MAX_PLAYERS;i++)
  			{
  		    	if(pInfo[i][Idioma] == 0)
	 				SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Cambiando al mapa {FF0000}Dust...");
	 			else
	 				SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Loading map {FF0000}Dust...");
			}
	 	}
	 	case iceworld:
		{
	 		MapaActual = iceworld;
	 		for(new i=0;i<MAX_PLAYERS;i++)
  			{
  		    	if(pInfo[i][Idioma] == 0)
	 				SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Cambiando al mapa {FF0000}Ice World...");
	 			else
	 				SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Loading map {FF0000}Ice World...");
			}
	 	}
	 	case detrain:
		{
	 		MapaActual = detrain;
	 		for(new i=0;i<MAX_PLAYERS;i++)
  			{
  		    	if(pInfo[i][Idioma] == 0)
	 				SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Cambiando al mapa {FF0000}Detrain...");
	 			else
	 				SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Loading map {FF0000}Detrain...");
			}
	 	}
	 	case assault:
		{
	 		MapaActual = assault;
	 		for(new i=0;i<MAX_PLAYERS;i++)
  			{
  		    	if(pInfo[i][Idioma] == 0)
	 				SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Cambiando al mapa {FF0000}Assault...");
	 			else
	 				SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Loading map {FF0000}Assault...");
			}
	 	}
	 	case inferno:
		{
	 		MapaActual = inferno;
	 		for(new i=0;i<MAX_PLAYERS;i++)
  			{
  		    	if(pInfo[i][Idioma] == 0)
	 				SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Cambiando al mapa {FF0000}Inferno...");
	 			else
	 				SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Loading map {FF0000}Inferno...");
			}
	 	}
	}
	TimerMatch = SetTimer("StartMatch",START_MATCH*1000,1);
	SendClientMessageToAll(COLOR_INFO,"[INFO]: {FFFFFF}Esperando suficientes jugadores para empezar la ronda...");
	printf("Cargando nuevo mapa %d:%s",mapa,idtoString(mapa,0));
	new mapname[24];
	format(mapname,sizeof(mapname),"mapname %s",idtoString(mapa,0));
	SendRconCommand(mapname);
}
forward SpawnTime(playerid);
public SpawnTime(playerid)
{
    if(pInfo[playerid][Idioma] == 0)
   		GameTextForPlayer(playerid,"~n~~n~~n~~n~~w~Mapa ~g~Cargado!",3000,4);
	else
   		GameTextForPlayer(playerid,"~n~~n~~n~~n~~w~Map ~g~Ready!",3000,4);
}
forward StartMatch();
public StartMatch()
{
	if(JugadoresEnEquipo(Terrorist) >= 1 && JugadoresEnEquipo(CTerrorist) >= 1)
	{
		PartidaInfo[EnPartida] = 1;
		KillTimer(TimerMatch);
		for(new i=0;i<MAX_PLAYERS;i++)
  		{
  		    if(pInfo[i][Idioma] == 0)
		    	SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}La ronda ah empezado!");
			else
		    	SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Match Started!");
		    	TogglePlayerControllable(i, 1);
		}
	}
	else
	{
		for(new i=0;i<MAX_PLAYERS;i++)
  		{
	    	if(pInfo[i][Idioma] == 0)
	    	SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Esperando suficientes jugadores para empezar la ronda...");
   		 	else
			SendClientMessage(i,COLOR_INFO,"[INFO]: {FFFFFF}Waiting for other players to start a match...");
 		}
	}
}
// _____________________________________________________________________________
stock SpawnPlayerMap(playerid)
{
	switch(MapaActual)
	{
    	case dust:
    	{
    		if(pInfo[playerid][cTeam] == Terrorist)
			{
		    new rand = random(sizeof(TerroristDust));
            SetPlayerPos(playerid, TerroristDust[rand][0], TerroristDust[rand][1],TerroristDust[rand][2]);
            SetPlayerFacingAngle(playerid, TerroristDust[rand][3]);
			}
			else if(pInfo[playerid][cTeam] == CTerrorist)
			{
		    new rand = random(sizeof(CounterTerroristDust));
            SetPlayerPos(playerid, CounterTerroristDust[rand][0], CounterTerroristDust[rand][1],CounterTerroristDust[rand][2]);
            SetPlayerFacingAngle(playerid, CounterTerroristDust[rand][3]);
		    }
	    }
	}
}
// _____________________________________________________________________________
stock JugadoresEnEquipo(teamid)
{
    new playercount = 0;
    for(new i = 0; i < MAX_PLAYERS; i++)
    {
        if(GetPlayerState(i) == PLAYER_STATE_NONE) continue;
        if(GetPlayerTeam(i) != teamid) continue;
        playercount++;
    }
    return playercount;
}
stock idtoString(id,type)
{
	new string[128];
	if(type == 0)
	{
		switch(id)
		{
		    case dust: string = "Dust";
		    case iceworld: string = "Ice World";
		    case detrain: string = "Detrain";
		    case assault: string = "Assault";
		    case inferno: string = "Inferno";
		}
	}
	else if(type == 1)
	{
		switch(id)
		{
		    case 0: string = "{E52828}TERRORIST";
		    case 1: string = "{6EA1DC}COUNTER TERRORIST";
		}
	}
	return string;
}
stock PlayerName(playerid)
{
	new Name123[MAX_PLAYERS];
	GetPlayerName(playerid, Name123, sizeof(Name123));
	return Name123;
}
